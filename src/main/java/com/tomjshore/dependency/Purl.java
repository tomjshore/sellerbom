package com.tomjshore.dependency;


import java.util.Locale;

/**
 * PURL reference <a href="https://github.com/package-url/purl-spec/blob/master/PURL-SPECIFICATION.rst">PURL</a>
 */
public record Purl(PurlType type, String namespace, String name, String version) {

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(100);
        builder.append("pkg:");
        builder.append(type.name().toLowerCase(Locale.ROOT));

        if(namespace != null){
            builder.append("/");
            builder.append(namespace);
        }
        builder.append("/");
        builder.append(name);

        if(version != null){
            builder.append("@");
            builder.append(version);
        }

        return builder.toString();
    }
}
