package com.tomjshore.dependency.resolver;

import com.tomjshore.DependencyManagementTool;
import com.tomjshore.dependency.Dependency;
import com.tomjshore.dependency.resolver.maven.MavenDependencyResolver;
import com.tomjshore.dependency.resolver.maven.MavenDependencyResolverFactory;
import com.tomjshore.dependency.resolver.npm.NpmDependencyResolver;
import com.tomjshore.dependency.resolver.npm.NpmDependencyResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.OperationNotSupportedException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;


import static com.tomjshore.DependencyManagementTool.MAVEN;
import static com.tomjshore.DependencyManagementTool.NPM;

/**
 * Factory for making {@link Resolver}
 */
public class DependencyResolverFactory {

  private final Logger LOG = LoggerFactory.getLogger(DependencyResolverFactory.class);
  private final Map<DependencyManagementTool, BuildableDependencyResolver<?>> factories;

  public DependencyResolverFactory() {
    factories = Map.of(MAVEN, new MavenDependencyResolverFactory(), NPM,
        new NpmDependencyResolverFactory());
  }

  /**
   * Builds a new instance of {@link Resolver}
   * 
   * @param executorService which is injected into {@link Resolver}
   * @param tool that is decide which subclass of {@link Resolver} will be made
   * @return new instance of {@link Resolver}
   * @throws OperationNotSupportedException if no subclass of {@link Resolver} is configured
   */
  public Resolver<Set<Dependency>> build(ExecutorService executorService,
      DependencyManagementTool tool) throws OperationNotSupportedException {
    BuildableDependencyResolver<?> factory = factories.get(tool);
    if (factory == null) {
      throw new OperationNotSupportedException();
    }
    return factory.build(executorService);

  }


}
