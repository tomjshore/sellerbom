package com.tomjshore.dependency.resolver;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * Resolves/finds something in a {@link CompletableFuture}
 * 
 * @param <T> the thing to resolve
 */
public interface Resolver<T> {

  CompletableFuture<T> resolve(Collection<File> interestingFiles);
}
