package com.tomjshore.dependency.resolver.maven;

import com.tomjshore.dependency.Dependency;
import com.tomjshore.dependency.Purl;
import com.tomjshore.dependency.resolver.Resolver;
import com.tomjshore.xml.SafeXml;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tomjshore.ChecksumType.MD5;
import static com.tomjshore.ChecksumType.SHA1;
import static com.tomjshore.dependency.PurlType.MAVEN;



public class MavenDependencyResolver implements Resolver<Set<Dependency>> {

  private static final Logger LOG = LoggerFactory.getLogger(MavenDependencyResolver.class);

  private static final String DEPENDENCIES_ELEMENT_NAME = "dependencies";
  private static final String PROPERTIES_ELEMENT_NAME = "properties";
  private static final String GROUP_ID_ELEMENT_NAME = "groupId";
  private static final String ARTIFACT_ID_ELEMENT_NAME = "artifactId";
  private static final String VERSION_ELEMENT_NAME = "version";
  private static final String SCOPE_ELEMENT_NAME = "scope";
  // Pattern that looks for Strings like ${foo} or ${junit.version}
  private static final Pattern MAVEN_STRING_REPLACEMENT = Pattern.compile("\\$\\{.*}");



  private Map<String, String> defaultProperties;
  private final ExecutorService executor;
  private final MavenRepositoryApi mavenRepoApi;

  private final Map<Purl, Dependency> mavenCache;

  public MavenDependencyResolver(ExecutorService executor, MavenRepositoryApi mavenRepoApi) {
    this.executor = executor;
    this.mavenRepoApi = mavenRepoApi;
    this.buildDefaultProperties();
    mavenCache = new HashMap<>();
  }

  @Override
  public CompletableFuture<Set<Dependency>> resolve(Collection<File> interestingFiles) {
    return CompletableFuture.supplyAsync(() -> findDependenciesInInterestingFiles(interestingFiles),
        executor);
  }

  private Set<Dependency> findDependenciesInInterestingFiles(Collection<File> interestingFiles) {
    return interestingFiles.stream().flatMap(pomAsFile -> {
      try {
        InputStream pom = new FileInputStream(pomAsFile);
        LOG.info("Found pom {}", pomAsFile.getPath());
        return findDependenciesInPom(pom);
      } catch (IOException e) {
        return Stream.empty();
      }
    }).collect(Collectors.toSet());
  }

  private Stream<Dependency> findDependenciesInPom(InputStream pomStream) {
    try {
      Document pom = SafeXml.saxReader().read(pomStream);

      Map<String, String> properties = buildProperties(pom.getRootElement());

      Stream<Dependency> parentDependencies =
          findParentDependencies(pom.getRootElement().element("parent"), properties)
              .filter(Objects::nonNull);
      Stream<Dependency> dependencies;
      Element dependenciesElement = pom.getRootElement().element(DEPENDENCIES_ELEMENT_NAME);
      if (dependenciesElement == null) {
        dependencies = Stream.empty();
      } else {
        dependencies = dependenciesElement.elements().stream()
            .map(dependency -> convertDependencyElement(dependency, properties))
            .filter(Objects::nonNull);
      }

      return Stream.concat(parentDependencies, dependencies);

    } catch (DocumentException e) {
      LOG.error("Could not parse pom ", e);
      return Stream.empty();
    }
  }

  private Map<String, String> buildProperties(Element rootElement) {
    Element propertiesElement = rootElement.element(PROPERTIES_ELEMENT_NAME);
    Element projectElement = rootElement;

    Map<String, String> projectProperties =
        projectElement.elements().stream().filter(Element::isTextOnly)
            .collect(Collectors.toMap(e -> "project." + e.getName(), Element::getStringValue));

    if (propertiesElement == null) {
      return Stream
          .concat(defaultProperties.entrySet().stream(), projectProperties.entrySet().stream())
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    Map<String, String> extraProperties = propertiesElement.elements().stream()
        .collect(Collectors.toMap(Element::getName, Element::getStringValue));


    Stream<Map.Entry<String, String>> combined = Stream.concat(
        Stream.concat(defaultProperties.entrySet().stream(), extraProperties.entrySet().stream()),
        projectProperties.entrySet().stream());
    return combined.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private void buildDefaultProperties() {
    int size = System.getProperties().size() + System.getenv().size();
    this.defaultProperties = new HashMap<>(size);
    System.getProperties()
        .forEach((key, value) -> defaultProperties.put(key.toString(), value.toString()));
    System.getenv().forEach((key, value) -> defaultProperties.put("env." + key, value));
  }

  private Dependency convertDependencyElement(Element dependencyAsXml,
      Map<String, String> properties) {
    if (isTestScope(dependencyAsXml)) {
      // skipping dependency if the scope is test because dependency that are exclusively used for
      // testing are not shipped to end users
      return null;
    }
    if (isOptional(dependencyAsXml)) {
      return null;
    }

    Purl purl = buildPurl(dependencyAsXml, properties);

    if (mavenCache.containsKey(purl)) {
      return null;
    }

    Dependency dependency = new Dependency(purl);
    mavenCache.put(purl, dependency);
    addInCheckSum(dependency);
    addInChildren(dependency);

    return dependency;
  }

  private boolean isTestScope(Element dependencyAsXml) {
    Element scope = dependencyAsXml.element(SCOPE_ELEMENT_NAME);
    if (scope == null) {
      return false;
    }
    return scope.getStringValue().equalsIgnoreCase("test");
  }

  private boolean isOptional(Element dependencyAsXml) {
    Element scope = dependencyAsXml.element("optional");
    if (scope == null) {
      return false;
    }
    return Boolean.parseBoolean(scope.getStringValue());
  }

  private Purl buildPurl(Element dependencyAsXml, Map<String, String> properties) {

    return new Purl(MAVEN,
        findTextInElement(dependencyAsXml.element(GROUP_ID_ELEMENT_NAME), properties),
        findTextInElement(dependencyAsXml.element(ARTIFACT_ID_ELEMENT_NAME), properties),
        findTextInElement(dependencyAsXml.element(VERSION_ELEMENT_NAME), properties));
  }

  private void addInCheckSum(Dependency dependency) {
    Purl purl = dependency.getPurl();
    this.mavenRepoApi.fetchCheckSum(purl.namespace(), purl.name(), purl.version())
        .ifPresent(checksum -> {
          dependency.setChecksum(SHA1, checksum.sha1());
          dependency.setChecksum(MD5, checksum.md5());
        });
  }

  private void addInChildren(Dependency dependency) {
    Purl purl = dependency.getPurl();
    this.mavenRepoApi.fetchPom(purl.namespace(), purl.name(), purl.version())
        .ifPresent(subPom -> findDependenciesInPom(subPom).forEach(dependency::addChild));
  }

  private Stream<Dependency> findParentDependencies(Element parent,
      Map<String, String> properties) {
    if (parent == null) {
      LOG.debug("No pom parent found");
      return Stream.empty();

    }
    Purl purl = buildPurl(parent, properties);
    LOG.info("pom parent purl {}", purl);
    Optional<InputStream> pom =
        this.mavenRepoApi.fetchPom(purl.namespace(), purl.name(), purl.version());
    if (pom.isPresent()) {
      return findDependenciesInPom(pom.get());
    }
    return Stream.empty();
  }

  private String findTextInElement(Element element, Map<String, String> properties) {
    if (element == null) {
      return null;
    }
    String fromXml = element.getStringValue();
    Matcher hasReplacementText = MAVEN_STRING_REPLACEMENT.matcher(fromXml);
    if (hasReplacementText.find() && hasReplacementText.groupCount() == 0) {
      String propertyName = hasReplacementText.group().replace("${", "").replace("}", "");
      String propertyValue = properties.get(propertyName);
      if (properties.containsKey(propertyName)) {
        return fromXml.replace("${" + propertyName + "}", propertyValue);
      }
    }
    return fromXml;
  }
}
