package com.tomjshore.dependency.resolver.maven;


import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;


import com.tomjshore.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.tomjshore.ChecksumType.MD5;
import static com.tomjshore.ChecksumType.SHA1;

/**
 * Sends Http messages to get data from a maven repository
 */
public class MavenRepositoryApi {

  private final static Logger LOG = LoggerFactory.getLogger(MavenRepositoryApi.class);

  private final URI centralRepoAddress;

  public MavenRepositoryApi(URI centralRepoAddress) {
    this.centralRepoAddress = centralRepoAddress;
  }

  /**
   * Finds the sha1 and md5 checksum of a given maven package.
   * 
   * @param groupId the group id of the package
   * @param artifactId the artifact id of the package
   * @param version the version of the package
   * @return the checksum if package exists on the maven repository or empty if does not exist
   */
  public Optional<MavenCheckSum> fetchCheckSum(String groupId, String artifactId, String version) {
    HttpClient httpClient = HttpClient.newBuilder().build();
    HttpRequest md5Request =
        buildChecksumRequest(groupId, artifactId, version, MD5.name().toLowerCase());
    HttpRequest sha1Request =
        buildChecksumRequest(groupId, artifactId, version, SHA1.name().toLowerCase());
    try {

      CompletableFuture<HttpResponse<String>> md5Response =
          httpClient.sendAsync(md5Request, HttpResponse.BodyHandlers.ofString());
      CompletableFuture<HttpResponse<String>> sha1Response =
          httpClient.sendAsync(sha1Request, HttpResponse.BodyHandlers.ofString());

      MavenCheckSum checkSum = sha1Response.thenCombine(md5Response, this::processChecksumResponses)
          .get(30, TimeUnit.SECONDS);
      return Optional.ofNullable(checkSum);

    } catch (Exception e) {
      LOG.error("Could not do http request so returning empty checksum", e);
      return Optional.empty();
    }
  }

  /**
   * Finds a pom from the maven repository for a given package.
   *
   * @param groupId the group id of the package
   * @param artifactId the artifact id of the package
   * @param version the version of the package
   * @return the pom if found or empty if not found
   */
  public Optional<InputStream> fetchPom(String groupId, String artifactId, String version) {
    HttpClient httpClient = HttpClient.newBuilder().build();
    HttpRequest pomRequest = buildPomRequest(groupId, artifactId, version);
    try {
      HttpResponse<InputStream> pomResponse =
          httpClient.send(pomRequest, HttpResponse.BodyHandlers.ofInputStream());
      if (pomResponse.statusCode() != HttpStatus.OK) {
        LOG.error("Could not get pom from {} status {}", pomResponse.request().uri(),
            pomResponse.statusCode());
        return Optional.empty();
      }
      return Optional.of(pomResponse.body());
    } catch (Exception e) {
      LOG.error("Could not do http request so returning empty pom", e);
      return Optional.empty();
    }
  }

  private MavenCheckSum processChecksumResponses(HttpResponse<String> sha1Response,
      HttpResponse<String> md5Response) {
    if (sha1Response.statusCode() == HttpStatus.OK && md5Response.statusCode() == HttpStatus.OK) {
      return new MavenCheckSum(sha1Response.body(), md5Response.body());
    }
    LOG.error("Could not get checksum from {} status {} : {} status {}",
        sha1Response.request().uri(), sha1Response.statusCode(), md5Response.request().uri(),
        md5Response.statusCode());
    return null;
  }

  private HttpRequest buildChecksumRequest(String groupId, String artifactId, String version,
      String checksum) {
    Optional<URI> uri = buildUri("%s/%s/%s/%s/%s-%s.jar." + checksum, groupId, artifactId, version);
    if (uri.isPresent()) {
      LOG.info("Pinging: {}", uri);
      return HttpRequest.newBuilder().GET().uri(uri.get()).build();
    }
    return null;

  }

  private HttpRequest buildPomRequest(String groupId, String artifactId, String version) {
    Optional<URI> uri = buildUri("%s/%s/%s/%s/%s-%s.pom", groupId, artifactId, version);
    if (uri.isPresent()) {
      LOG.info("Pinging: {}", uri);
      return HttpRequest.newBuilder().GET().uri(uri.get()).build();
    }
    return null;
  }

  private Optional<URI> buildUri(String format, String groupId, String artifactId, String version) {
    String groupIdPath = groupId.replaceAll("\\.", "/");
    String address = String.format(format, centralRepoAddress.toString(), groupIdPath, artifactId,
        version, artifactId, version);
    try {
      return Optional.of(new URI(address));
    } catch (URISyntaxException e) {
      LOG.error("{} is not a valid web address {} {} {} is not a valid maven package", e.getInput(),
          groupId, artifactId, version);
      return Optional.empty();
    }
  }


}
