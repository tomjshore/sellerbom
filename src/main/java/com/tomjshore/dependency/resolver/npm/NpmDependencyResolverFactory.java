package com.tomjshore.dependency.resolver.npm;

import com.tomjshore.dependency.resolver.BuildableDependencyResolver;

import java.util.concurrent.ExecutorService;

public class NpmDependencyResolverFactory
    implements BuildableDependencyResolver<NpmDependencyResolver> {
  @Override
  public NpmDependencyResolver build(ExecutorService executor) {
    return new NpmDependencyResolver(executor);
  }
}
