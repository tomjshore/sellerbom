package com.tomjshore.file;

import com.tomjshore.DependencyManagementTool;
import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Tries to find files that are of interest to dependency management tool.
 */
public class FileFinder {

  private final Map<Pattern, DependencyManagementTool> patterns;

  public FileFinder(Map<Pattern, DependencyManagementTool> patterns) {
    this.patterns = patterns;
  }

  /**
   * Looks recursively for patterns in file names and maps them to
   * {@link com.tomjshore.file.DependencyFile}
   * 
   * @param rootAddress The root of where the source code is
   * @return files of interest
   */
  public Collection<DependencyFile> find(String rootAddress) {
    File root = new File(rootAddress);

    if (!root.exists()) {
      return List.of();
    }

    return recursivelyFind(root);
  }

  private Collection<DependencyFile> recursivelyFind(File parent) {
    List<DependencyFile> filesFound = new ArrayList<>();
    File[] subFiles = parent.listFiles();
    if (subFiles == null) {
      return filesFound;
    }

    Arrays.stream(subFiles).forEach(file -> {
      if (file.isDirectory()) {
        filesFound.addAll(recursivelyFind(file));
      } else {
        Optional<DependencyFile> dependencyFile = findDependencyFile(file);
        dependencyFile.ifPresent(filesFound::add);
      }
    });

    return filesFound;
  }

  private Optional<DependencyFile> findDependencyFile(File file) {
    for (Pattern pattern : patterns.keySet()) {
      if (pattern.matcher(file.getName()).matches()) {
        return Optional.of(new DependencyFile(file, patterns.get(pattern)));
      }
    }
    return Optional.empty();
  }
}
