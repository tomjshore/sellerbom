package com.tomjshore.file;

import com.tomjshore.DependencyManagementTool;
import com.tomjshore.xml.SafeXml;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Builder for {@link com.tomjshore.file.FileFinder}
 */
public class FileFinderBuilder {

  private final static Logger LOG = LoggerFactory.getLogger(FileFinderBuilder.class);
  private final static String FILE_CONFIG_LOCATION = "/FileFinderConfig.xml";
  private final static String PATTERN_XML_NODE = "pattern";
  private final static String TOOL_XML_NODE = "tool";

  private final Map<Pattern, DependencyManagementTool> patterns;

  public FileFinderBuilder() {
    patterns = loadPatternsMap();
  }

  private FileFinderBuilder(Map<Pattern, DependencyManagementTool> patterns) {
    this.patterns = patterns;
  }


  /**
   * Builds {@link com.tomjshore.file.FileFinder}
   * 
   * @return a new instance of {@link com.tomjshore.file.FileFinder}
   */
  public FileFinder build() {
    return new FileFinder(patterns);
  }

  /**
   * Puts a new pattern into a new {@link com.tomjshore.file.FileFinderBuilder}
   * 
   * @param pattern regex of file names to look at
   * @param tool the tool that the found file will be associate with
   * @return a new instance of {@link com.tomjshore.file.FileFinderBuilder} with the added pattern
   */
  public FileFinderBuilder appendPattern(Pattern pattern, DependencyManagementTool tool) {
    Map<Pattern, DependencyManagementTool> appendedPatterns = new HashMap<>(this.patterns);
    appendedPatterns.put(pattern, tool);
    return new FileFinderBuilder(appendedPatterns);
  }

  private Map<Pattern, DependencyManagementTool> loadPatternsMap() {
    Map<Pattern, DependencyManagementTool> patternMap = new HashMap<>();
    SAXReader reader = SafeXml.saxReader();
    try {
      Document configFile = reader.read(this.getClass().getResourceAsStream(FILE_CONFIG_LOCATION));
      Iterator<Element> entries = configFile.getRootElement().elementIterator();
      while (entries.hasNext()) {
        putEntityIntoMap(patternMap, entries.next());
      }
    } catch (DocumentException e) {
      LOG.error("Could not load xml to configure FileFinderBuilder from classpath {}",
          FILE_CONFIG_LOCATION, e);
      System.exit(1);
    }
    return patternMap;
  }

  private void putEntityIntoMap(Map<Pattern, DependencyManagementTool> patternMap,
      Element xmlEntity) {
    Element patternElement = xmlEntity.element(PATTERN_XML_NODE);
    Element toolElement = xmlEntity.element(TOOL_XML_NODE);
    Pattern pattern = Pattern.compile(patternElement.getStringValue());
    patternMap.put(pattern, DependencyManagementTool.valueOf(toolElement.getStringValue()));
  }
}
