package com.tomjshore.report;

import java.util.Arrays;

public enum ReportVersion {
  CYCLONEDX_LATEST("1.4"), SPDX_LATEST("2.2"), ONE_FOUR("1.4"), ONE_THREE("1.3"), TWO_TWO("2.2");

  private final String version;

  ReportVersion(String version) {
    this.version = version;
  }

  public static ReportVersion defaultVersion(ReportType type) {
    if (ReportType.CYCLONEDX == type) {
      return CYCLONEDX_LATEST;
    }
    return SPDX_LATEST;
  }

  public static ReportVersion fromArg(String arg) {
    return Arrays.stream(ReportVersion.values()).filter(rv -> arg.equals(rv.version)).findFirst()
        .orElseThrow(() -> new IllegalArgumentException(arg + " is not a valid report version"));
  }

  @Override
  public String toString() {
    return this.version;
  }
}
