package com.tomjshore.start;

import com.tomjshore.report.ReportType;
import com.tomjshore.report.ReportVersion;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.event.Level;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ArgumentParser {

  private static final String OUTPUT_FILE_FLAG = "o";
  private static final String OUTPUT_FILE_DEFAULT = "output.xml";
  private static final String OUTPUT_FILE_DESCRIPTION =
      "Output file name.eg:bob.json default:%s".formatted(OUTPUT_FILE_DEFAULT);

  private static final String REPORT_TYPE_FLAG = "t";
  private static final ReportType REPORT_TYPE_DEFAULT = ReportType.CYCLONEDX;
  private static final String REPORT_TYPE_DESCRIPTION = "Report type can be %s default:%s"
      .formatted(enumsToString(ReportType.values()), REPORT_TYPE_DEFAULT);

  private static final String REPORT_VERSION_FLAG = "s";
  private static final String REPORT_VERSION_DESCRIPTION = "Report version to be output";

  private static final String HELP_FLAG = "h";
  private static final String HELP_DESCRIPTION = "The text you are reading";

  private static final String VERSION_FLAG = "v";
  private static final String VERSION_DESCRIPTION = "Shows the version of SellerBOM";

  private static final String VERBOSITY_FLAG = "verbosity";
  private static final Level VERBOSITY_DEFAULT = Level.INFO;
  private static final String VERBOSITY_DESCRIPTION =
      "The log level of verbosity can be %s default: %s".formatted(enumsToString(Level.values()),
          VERBOSITY_DEFAULT.toString());

  private final CommandLineParser commandLineParser;
  private final Options options;

  public ArgumentParser() {
    options = new Options();
    options.addOption(Option.builder(OUTPUT_FILE_FLAG).required(false).hasArg(true)
        .desc(OUTPUT_FILE_DESCRIPTION).build());
    options.addOption(Option.builder(REPORT_TYPE_FLAG).required(false).hasArg(true)
        .desc(REPORT_TYPE_DESCRIPTION).build());
    options.addOption(Option.builder(REPORT_VERSION_FLAG).required(false).hasArg(true)
        .desc(REPORT_VERSION_DESCRIPTION).build());
    options.addOption(
        Option.builder(HELP_FLAG).required(false).hasArg(false).desc(HELP_DESCRIPTION).build());
    options.addOption(Option.builder(VERSION_FLAG).required(false).hasArg(false)
        .desc(VERSION_DESCRIPTION).build());
    options.addOption(Option.builder().longOpt(VERBOSITY_FLAG).required(false).hasArg(true)
        .desc(VERBOSITY_DESCRIPTION).build());

    commandLineParser = new DefaultParser();
  }

  /**
   * Builds the {@link Arguments} object from user input
   * 
   * @param inputArguments input passed in from the command line
   * @return a new {@link Arguments} object with default if user has not said
   */
  Arguments parse(String[] inputArguments) {
    try {
      CommandLine commandLine = commandLineParser.parse(options, inputArguments);
      return build(commandLine);
    } catch (ParseException e) {
      return buildShowHelp(List.of(e.getMessage()));
    }
  }

  private Arguments build(CommandLine commandLine) {
    if (commandLine.hasOption(HELP_FLAG)) {
      return buildShowHelp(List.of());
    }

    if (commandLine.hasOption(VERSION_FLAG)) {
      return buildShowVersion();
    }

    Collection<String> errors = new LinkedList<>();
    File output = new File(OUTPUT_FILE_DEFAULT);
    ReportType reportType = REPORT_TYPE_DEFAULT;
    ReportVersion reportVersion = null;
    Level level = VERBOSITY_DEFAULT;

    if (commandLine.hasOption(OUTPUT_FILE_FLAG)) {
      output = new File(commandLine.getOptionValue(OUTPUT_FILE_FLAG));
      if (!output.canWrite()) {
        errors.add("Cannot write to file %s".formatted(output.getAbsolutePath()));
      }
    }

    if (commandLine.hasOption(REPORT_TYPE_FLAG)) {
      try {
        reportType = ReportType
            .valueOf(commandLine.getOptionValue(REPORT_TYPE_FLAG).toUpperCase(Locale.ROOT));
      } catch (IllegalArgumentException e) {
        errors.add("%s is not a Report Type choose one of %s".formatted(
            commandLine.getOptionValue(REPORT_TYPE_FLAG), enumsToString(ReportType.values())));
      }
    }

    if (commandLine.hasOption(REPORT_VERSION_FLAG)) {
      try {
        reportVersion = ReportVersion.fromArg(commandLine.getOptionValue(REPORT_VERSION_FLAG));
      } catch (IllegalArgumentException e) {
        errors.add(e.getMessage());
      }
    } else {
      reportVersion = ReportVersion.defaultVersion(reportType);
    }

    if (commandLine.hasOption(VERBOSITY_FLAG)) {
      try {
        level = Level.valueOf(commandLine.getOptionValue(VERBOSITY_FLAG).toUpperCase(Locale.ROOT));
      } catch (IllegalArgumentException e) {
        errors.add("%s is not a Log Level choose one of %s"
            .formatted(commandLine.getOptionValue(VERBOSITY_FLAG), enumsToString(Level.values())));
      }
    }

    return new Arguments(output, reportType, reportVersion, false, !errors.isEmpty(), errors,
        level);
  }

  private Arguments buildShowHelp(List<String> error) {
    return new Arguments(null, null, null, false, true, error, null);
  }

  private Arguments buildShowVersion() {
    return new Arguments(null, null, null, true, false, List.of(), null);
  }

  private static String enumsToString(Enum<?>[] enums) {
    return Arrays.stream(enums).map(Enum::toString).collect(Collectors.joining(", "));
  }
}
