package com.tomjshore.start;

import com.tomjshore.report.ReportType;
import com.tomjshore.report.ReportVersion;
import org.slf4j.event.Level;

import java.io.File;
import java.util.Collection;

public record Arguments(File output, ReportType reportType, ReportVersion reportVersion, boolean showVersion, boolean showHelp, Collection<String> errors, Level logLevel) {

}
