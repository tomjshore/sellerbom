package com.tomjshore.dependency.resolver;

import com.tomjshore.DependencyManagementTool;
import com.tomjshore.dependency.Dependency;
import com.tomjshore.dependency.resolver.maven.MavenDependencyResolver;
import com.tomjshore.dependency.resolver.npm.NpmDependencyResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.naming.OperationNotSupportedException;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertTrue;

class DependencyResolverFactoryTest {

  private DependencyResolverFactory factory;
  private ExecutorService executorService;

  @BeforeEach
  void setUp() {
    factory = new DependencyResolverFactory();
    executorService = Executors.newSingleThreadExecutor();
  }

  @Test
  void testBuildReturnsMavenCompatibleDependencyResolverWhenMavenIsPassedIn()
      throws OperationNotSupportedException {
    // when
    Resolver<Set<Dependency>> resolver =
        factory.build(executorService, DependencyManagementTool.MAVEN);

    // then
    assertTrue(resolver instanceof MavenDependencyResolver);
  }

  @Test
  void testBuildReturnsNpmCompatibleDependencyResolverWhenNpmIsPassedIn()
      throws OperationNotSupportedException {
    // when
    Resolver<Set<Dependency>> resolver =
        factory.build(executorService, DependencyManagementTool.NPM);

    // then
    assertTrue(resolver instanceof NpmDependencyResolver);
  }
}
