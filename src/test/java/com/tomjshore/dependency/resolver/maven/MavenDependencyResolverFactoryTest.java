package com.tomjshore.dependency.resolver.maven;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class MavenDependencyResolverFactoryTest {

  @Test
  void testBuildReturnsMavenDependencyResolver() {
    // given
    MavenDependencyResolverFactory factory = new MavenDependencyResolverFactory();
    // when
    MavenDependencyResolver resolver = factory.build(Executors.newSingleThreadExecutor());
    // then
    assertNotNull(resolver);
  }
}
