package com.tomjshore.dependency.resolver.maven;

import com.tomjshore.dependency.Dependency;
import com.tomjshore.dependency.Purl;
import com.tomjshore.dependency.PurlType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.*;


import static com.tomjshore.ChecksumType.MD5;
import static com.tomjshore.ChecksumType.SHA1;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@ExtendWith(MockitoExtension.class)
class MavenDependencyResolverTest {

  @Mock
  private MavenRepositoryApi api;

  private MavenDependencyResolver resolver;

  @BeforeEach
  void setUp() {
    resolver = new MavenDependencyResolver(Executors.newSingleThreadExecutor(), api);
  }

  @Test
  void testResolveReturnsOneDependencyWhenGivenPomWithOneDependencyAndNoChild() throws Exception {
    // given
    List<File> pom = getPom("/maven/pom-one-dep-no-child.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    assertEquals(1, dependencies.size());
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithOneDependencyAndNoChild() throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));
    List<File> pom = getPom("/maven/pom-one-dep-no-child.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWithChecksum() throws Exception {
    // given
    String sha1 = "ff2673ba4499942b1afc5fc1cb4147dd803a633b";
    String md5 = "23ca795969de5245040491a3f599aecd";
    MavenCheckSum checkSum = new MavenCheckSum(sha1, md5);
    List<File> pom = getPom("/maven/pom-one-dep-no-child.xml");
    given(api.fetchCheckSum("org.opentest4j", "opentest4j", "1.2.0"))
        .willReturn(Optional.of(checkSum));

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(sha1, dependency.getChecksum(SHA1));
    assertEquals(md5, dependency.getChecksum(MD5));

  }

  @Test
  void testResolveReturnsJunitWithHamcrestAsChild() throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "junit", "junit", "4.13.2"));
    Dependency expectedChildDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.hamcrest", "hamcrest-core", "1.3"));
    expectedDependency.addChild(expectedChildDependency);
    List<File> pom = getPom("/maven/pom-one-dep-with-child.xml");
    given(api.fetchPom("org.hamcrest", "hamcrest-core", "1.3"))
        .willReturn(Optional.of(getPomInputStream("/maven/pom-no-dep.xml")));
    given(api.fetchPom("junit", "junit", "4.13.2"))
        .willReturn(Optional.of(getPomInputStream("/maven/pom-junit.xml")));

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveIgnoresDependenciesWhichScopeOfTest() throws Exception {
    // given
    List<File> pom = getPom("/maven/pom-test-dep.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    assertTrue(dependencies.isEmpty());
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithOneDependencyAndVersionProperty()
      throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));
    List<File> pom = getPom("/maven/pom-one-dep-with-props.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithOneDependencyAndSystemProperty()
      throws Exception {
    // given
    String version = System.getProperty("java.version");
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", version));
    List<File> pom = getPom("/maven/pom-one-dep-with-system-props.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithOneDependencyAndEnvironmentProperty()
      throws Exception {
    // given
    String user = System.getenv("USER");
    assumeTrue(user != null);
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", user));
    List<File> pom = getPom("/maven/pom-one-dep-with-env-props.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithParent() throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));
    List<File> pom = getPom("/maven/pom-with-simple-parent.xml");

    given(api.fetchPom("testing", "simple-parent", "1.0.0"))
        .willReturn(Optional.of(getPomInputStream("/maven/parent/pom-with-simple-parent.xml")));

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithParentAndPropertiesInParent() throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));
    List<File> pom = getPom("/maven/pom-with-simple-parent.xml");

    given(api.fetchPom("testing", "simple-parent", "1.0.0")).willReturn(
        Optional.of(getPomInputStream("/maven/parent/pom-with-parent-that-has-properties.xml")));

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  @Test
  void testResolveReturnsOpenTest4jWhenGivenPomWithOneDependencyAndProjectProperty()
      throws Exception {
    // given
    Dependency expectedDependency =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));
    List<File> pom = getPom("/maven/pom-one-dep-with-project-props.xml");

    // when
    Set<Dependency> dependencies = resolver.resolve(pom).get(10, TimeUnit.SECONDS);

    // then
    Dependency dependency = dependencies.stream().findAny().orElseThrow();
    assertEquals(expectedDependency, dependency);
  }

  private List<File> getPom(String address) {
    File pom = new File(this.getClass().getResource(address).getPath());
    return List.of(pom);
  }

  private InputStream getPomInputStream(String address) throws IOException {
    return this.getClass().getResource(address).openStream();
  }
}
