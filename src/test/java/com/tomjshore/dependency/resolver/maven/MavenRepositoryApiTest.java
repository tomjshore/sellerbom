package com.tomjshore.dependency.resolver.maven;



import com.tomjshore.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockserver.client.MockServerClient;
import org.mockserver.junit.jupiter.MockServerExtension;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.stream.Collectors;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ExtendWith(MockServerExtension.class)
class MavenRepositoryApiTest {

  private MockServerClient mockServer;

  private MavenRepositoryApi api;

  @BeforeEach
  void setUp(MockServerClient mockServer) throws URISyntaxException {
    this.mockServer = mockServer;
    this.api =
        new MavenRepositoryApi(new URI("http://localhost:" + mockServer.getPort() + "/maven2"));
  }


  @Test
  void testFetchCheckSumReturnsMd5AndSha1OnValidPackage() {
    // given
    String groupId = "org.opentest4j";
    String artifactId = "opentest4j";
    String version = "1.2.0";

    String expectedMd5 = "45c9a837c21f68e8c93e85b121e2fb90";
    String expectedSha1 = "28c11eb91f9b6d8e200631d46e20a7f407f2a046";

    mockServer
        .when(
            request().withPath("/maven2/org/opentest4j/opentest4j/1.2.0/opentest4j-1.2.0.jar.sha1"))
        .respond(response().withBody(expectedSha1));
    mockServer
        .when(
            request().withPath("/maven2/org/opentest4j/opentest4j/1.2.0/opentest4j-1.2.0.jar.md5"))
        .respond(response().withBody(expectedMd5));

    // when
    Optional<MavenCheckSum> checkSum = api.fetchCheckSum(groupId, artifactId, version);

    // then
    assertTrue(checkSum.isPresent());
    assertEquals(expectedMd5, checkSum.get().md5());
    assertEquals(expectedSha1, checkSum.get().sha1());
  }

  @Test
  void testFetchCheckSumReturnNoneWhen404Happens() {
    // given
    String groupId = "com.notagroup";
    String artifactId = "notapackage";
    String version = "0.0.1";

    mockServer
        .when(request()
            .withPath("/maven2/com/notagroup/notapackage/0.0.1/notapackage-0.0.1.jar.sha1"))
        .respond(response().withStatusCode(HttpStatus.NOT_FOUND));
    mockServer
        .when(
            request().withPath("/maven2/com/notagroup/notapackage/0.0.1/notapackage-0.0.1.jar.md5"))
        .respond(response().withStatusCode(HttpStatus.NOT_FOUND));

    // when
    Optional<MavenCheckSum> checkSum = api.fetchCheckSum(groupId, artifactId, version);

    // then
    assertFalse(checkSum.isPresent());
  }

  @Test
  void testFetchCheckSumReturnsNoneWhenGivenAWeirdGroupId() {
    // given
    String groupId = "££££:)££££()()()";
    String artifactId = "^^^^^^^^^^^^";
    String version = "";

    // when
    Optional<MavenCheckSum> checkSum = api.fetchCheckSum(groupId, artifactId, version);

    // then
    assertFalse(checkSum.isPresent());
  }

  @Test
  void testFetchPomReturnsPomWhenGivenValidMavenPackage() throws IOException {
    //given
    String groupId = "org.opentest4j";
    String artifactId = "opentest4j";
    String version = "1.2.0";

    String expectedPom = """
            <?xml version="1.0" encoding="UTF-8"?>
            <project xmlns="http://maven.apache.org/POM/4.0.0"
                     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                     xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
                <modelVersion>4.0.0</modelVersion>
                        
                <groupId>%s</groupId>
                <artifactId>%s</artifactId>
                <version>%s</version>
                <description>
                  This is a project
                </description>
            </project>
            """.formatted(groupId, artifactId, version).trim();
    mockServer
            .when(
                    request().withPath("/maven2/org/opentest4j/opentest4j/1.2.0/opentest4j-1.2.0.pom"))
            .respond(response().withBody(expectedPom));
    //when
    Optional<InputStream> pom = api.fetchPom(groupId, artifactId, version);

    //then
    assertTrue(pom.isPresent());
    String result = new String(pom.get().readAllBytes(), StandardCharsets.UTF_8);
    assertEquals(expectedPom, result);
  }

  @Test
  void testFetchPomReturnsEmptyWhen404() {
    // given
    String groupId = "com.notagroup";
    String artifactId = "notapackage";
    String version = "0.0.1";
    mockServer
        .when(request().withPath("/maven2/com/notagroup/notapackage/0.0.1/notapackage-0.0.1.pom"))
        .respond(response().withStatusCode(HttpStatus.NOT_FOUND));
    // when
    Optional<InputStream> pom = api.fetchPom(groupId, artifactId, version);

    // then
    assertFalse(pom.isPresent());
  }

  @Test
  void testFetchCheckSumReturnsEmptyWhenGivenAWeirdGroupId() {
    // given
    String groupId = "££££:)££££()()()";
    String artifactId = "^^^^^^^^^^^^";
    String version = "";

    // when
    Optional<InputStream> pom = api.fetchPom(groupId, artifactId, version);

    // then
    assertFalse(pom.isPresent());
  }
}
