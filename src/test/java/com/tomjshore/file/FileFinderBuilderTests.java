package com.tomjshore.file;

import com.tomjshore.DependencyManagementTool;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;

class FileFinderBuilderTests {

  @Test
  void testBuildReturnsFileFinder() throws Exception {
    // given
    FileFinderBuilder builder = new FileFinderBuilder();

    // when
    FileFinder finder = builder.build();

    // then
    assertNotNull(finder);
  }

  @Test
  void testBuilderReturnFileFinderThatCanFindPom() throws Exception {
    // given
    FileFinderBuilder builder = new FileFinderBuilder();

    // when
    FileFinder finder = builder.build();

    // then
    Map<Pattern, DependencyManagementTool> patterns = fetchPatterns(finder);
    assertTrue(patterns.containsValue(DependencyManagementTool.MAVEN));
    patterns.forEach((pattern, tool) -> {
      if (tool.equals(DependencyManagementTool.MAVEN)) {
        assertTrue(pattern.matcher("pom.xml").find());
      }
    });
  }


  @Test
  void testBuilderReturnsFileFinderThatCanFindPackageJson() throws Exception {
    // given
    FileFinderBuilder builder = new FileFinderBuilder();

    // when
    FileFinder finder = builder.build();

    // then
    Map<Pattern, DependencyManagementTool> patterns = fetchPatterns(finder);
    assertTrue(patterns.containsValue(DependencyManagementTool.NPM));
    patterns.forEach((pattern, tool) -> {
      if (tool.equals(DependencyManagementTool.NPM)) {
        assertTrue(pattern.matcher("package.json").find());
        assertTrue(pattern.matcher("package-lock.json").find());
      }
    });
  }

  @Test
  void testAppendPatternReturnsBuildWithExtraPattern()
      throws NoSuchFieldException, IllegalAccessException {
    // given
    Pattern someRegex = Pattern.compile("some regex");
    FileFinderBuilder builder = new FileFinderBuilder();

    // when
    builder = builder.appendPattern(someRegex, DependencyManagementTool.NPM);

    // then
    FileFinder finder = builder.build();
    Map<Pattern, DependencyManagementTool> patterns = fetchPatterns(finder);
    assertTrue(patterns.containsKey(someRegex));

  }

  private Map<Pattern, DependencyManagementTool> fetchPatterns(FileFinder fileFinder)
      throws NoSuchFieldException, IllegalAccessException {
    Field patterns = fileFinder.getClass().getDeclaredField("patterns");
    patterns.setAccessible(true);
    return (Map<Pattern, DependencyManagementTool>) patterns.get(fileFinder);
  }


}
