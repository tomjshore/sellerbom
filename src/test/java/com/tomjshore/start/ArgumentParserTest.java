package com.tomjshore.start;

import com.tomjshore.report.ReportType;
import com.tomjshore.report.ReportVersion;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.slf4j.event.Level;

class ArgumentParserTest {
  private ArgumentParser argumentParser;

  @BeforeEach
  void setUp() {
    argumentParser = new ArgumentParser();
  }

  @Test
  void testNoOutputFileHasDefaultOutputFile() throws IOException {
    // given
    String expectedOutputFileLocation =
        System.getProperty("user.dir") + File.separator + "output.xml";
    String[] args = new String[0];

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedOutputFileLocation, arguments.output().getCanonicalPath());
  }

  @Test
  void testCustomOutputFileHasCustomOutputFile() throws IOException {
    // given
    File tmpFile = File.createTempFile("custom", ".json");
    tmpFile.deleteOnExit();
    String expectedOutputFileLocation = tmpFile.getCanonicalPath();
    String[] args = new String[] {"-o", expectedOutputFileLocation};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedOutputFileLocation, arguments.output().getCanonicalPath());
  }

  @Test
  @EnabledOnOs({OS.LINUX})
  void testShowHelpWhenCannotWriteToOutput() {
    // given
    String[] args = new String[] {"-o", "/etc/shadow"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showHelp());
    assertTrue(arguments.errors().contains("Cannot write to file /etc/shadow"));
  }

  @Test
  void testNoReportTypeHasCycloneDX() {
    // given
    ReportType expectedType = ReportType.CYCLONEDX;
    String[] args = new String[0];

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedType, arguments.reportType());
  }

  @Test
  void testSPDXReportTypeHasSPDX() {
    // given
    ReportType expectedType = ReportType.SPDX;
    String[] args = new String[] {"-t", "SPDX"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedType, arguments.reportType());
  }

  @Test
  void testspdxReportTypeHasSPDX() {
    // given
    ReportType expectedType = ReportType.SPDX;
    String[] args = new String[] {"-t", "spdx"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedType, arguments.reportType());
  }

  @Test
  void testBadReportTypeHasShowHelpAndError() {
    // given
    String[] args = new String[] {"-t", "Not a report type"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showHelp());
    assertEquals(1, arguments.errors().size());
  }

  @Test
  void testShowHelpIfArgsAreRubbish() {
    // given
    String[] args = new String[] {"-z", "not", "good", "args"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showHelp());
    assertEquals(1, arguments.errors().size());
  }

  @Test
  void testShowHelpWhenHelpIsPassedIn() {
    // given
    String[] args = new String[] {"-h"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showHelp());
    assertEquals(0, arguments.errors().size());
  }

  @Test
  void testShowVersionIsTrueWhenUserAsksForVersion() {
    // given
    String[] args = new String[] {"-v"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showVersion());
  }

  @Test
  void testVerbosityIsInfoByDefault() {
    // given
    Level expectedLevel = Level.INFO;
    String[] args = new String[] {};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedLevel, arguments.logLevel());
  }

  @Test
  void testVerbosityIsTraceWhenSetByUser() {
    // given
    Level expectedLevel = Level.TRACE;
    String[] args = new String[] {"--verbosity", "TRACE"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedLevel, arguments.logLevel());
  }

  @Test
  void testVerbosityIsErrorWhenSetByUserInLowercase() {
    // given
    Level expectedLevel = Level.ERROR;
    String[] args = new String[] {"--verbosity", "error"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expectedLevel, arguments.logLevel());
  }

  @Test
  void testVerbositySetToRubbishHasErrors() {
    // given
    String[] args = new String[] {"--verbosity", "not a level"};

    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertTrue(arguments.showHelp());
    assertEquals(1, arguments.errors().size());
  }

  @Test
  void testReportVersionIsLatestCycloxDXWhenNotSet() {
    // given
    ReportVersion expected = ReportVersion.CYCLONEDX_LATEST;
    String[] args = new String[] {"-t", "CYCLONEDX"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expected, arguments.reportVersion());
  }

  @Test
  void testReportVersionIsLatestXWhenNothingIsSet() {
    // given
    ReportVersion expected = ReportVersion.CYCLONEDX_LATEST;
    String[] args = new String[] {};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expected, arguments.reportVersion());
  }

  @Test
  void testReportVersionIsLatestSPDXWhenNotSet() {
    // given
    ReportVersion expected = ReportVersion.SPDX_LATEST;
    String[] args = new String[] {"-t", "SPDX"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expected, arguments.reportVersion());
  }

  @Test
  void testReportVersionIs1Point3WhenUserSetsTo1Point3() {
    // given
    ReportVersion expected = ReportVersion.ONE_THREE;
    String[] args = new String[] {"-t", "CYCLONEDX", "-s", "1.3"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expected, arguments.reportVersion());
  }

  @Test
  void testReportVersionIs1Point3WhenUserSetsTo1Point3AndNoReportType() {
    // given
    ReportVersion expected = ReportVersion.ONE_THREE;
    String[] args = new String[] {"-s", "1.3"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(expected, arguments.reportVersion());
  }

  @Test
  void testHasErrorWhenReportVersionIsInvalid() {
    // given
    String[] args = new String[] {"-s", "not a version"};
    // when
    Arguments arguments = argumentParser.parse(args);

    // then
    assertEquals(1, arguments.errors().size());
  }

}
